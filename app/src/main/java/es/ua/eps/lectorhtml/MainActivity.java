package es.ua.eps.lectorhtml;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private static final int INTERNET_PERMISSION = 1;

    Button obtainButton;
    EditText urlText;
    TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        urlText = (EditText) findViewById(R.id.etUrl);
        content = (TextView) findViewById(R.id.tvContenido);
        obtainButton = (Button) findViewById(R.id.btnAcceder);

        obtainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContent();
            }
        });
    }

    private void getContent() {
        String url = urlText.getText().toString();
        new GetContentTask().execute(url);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case INTERNET_PERMISSION:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("PERMISSION", "Internet permission granted");
                    getContent();
                }
                break;
        }
    }

    class GetContentTask extends AsyncTask<String, Void, String> {

        private String downloadContent(String strUrl) {
            HttpURLConnection http = null;
            String content = null;
            try {
                URL url = new URL(strUrl);
                http = (HttpURLConnection) url.openConnection();
                if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(http.getInputStream()));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    content = sb.toString();
                    reader.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (http != null) http.disconnect();
            }

            return content;
        }

        @Override
        protected String doInBackground(String... urls) {
            return downloadContent(urls[0]);
        }

        @Override
        protected void onCancelled() {
            obtainButton.setEnabled(true);
        }

        @Override
        protected void onPreExecute() {
            obtainButton.setEnabled(false);
        }

        @Override
        protected void onPostExecute(String s) {
            obtainButton.setEnabled(true);
            content.setText(s);
        }
    }

}


